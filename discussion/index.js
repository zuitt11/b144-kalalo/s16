// Repetition Control Structures
// 	"Loops"; executes code repeatedly in a pre-set number of time or maybe forever

// While Loop - takes in an expression/condition before proceeding in the evaluation of the codes.
/*
	while (expression/condition) {
		statement/s;
	}
*/

let count = 5;

while(count !== 0) {
	console.log ("While loop: " + count);

	count--;
}

/*
	count
	5 4 3 2 1 0
*/


let countTwo = 1;

while (countTwo < 11) {
	console.log (countTwo);

	countTwo++;
}


// do-while loop - at least 1 code block will be executed before proceeding to the condition.
/*
	do {
		statement/s;
	} while (condition/expression)
*/

let countThree = 5;

do{
	console.log ("Do-While loop: " + countThree);

	countThree--;
} while(countThree > 0);



let countFour = 1;

do{
	console.log (countFour);

	countFour++;
} while(countFour < 11);


//For-loop - more flexible looping construct
/*
	for (initialization; expression/condition **then runs the statement**; finalExpression){
		statement/s;
	}
*/

for (let countFive = 5; countFive > 0; countFive--){
	console.log("For loop: " + countFive)
}


/*let number = Number(prompt("Give me a number."));

for (let numCount = 1; numCount <= number; numCount++) {
	console.log("Hello Batch 144");
}*/

let myString = "alex"
/*console.log(myString.length);

console.log(myString[2]);*/

for (let x = 0; x < myString.length; x++) {
	console.log(myString[x]);
}

let myName = 'Alex'
for (let i = 0; i < myName.length; i++) {
	if (myName[i].toLowerCase() == 'a' ||
		myName[i].toLowerCase() == 'e' ||
		myName[i].toLowerCase() == 'i' ||
		myName[i].toLowerCase() == 'o' ||
		myName[i].toLowerCase() == 'u'
	){
		console.log(3);
	}else {
		console.log(myName[i]);
	}
}


// Continue and Break statements
/*Continue - allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block
Break - terminates a loop once a match has been found
*/

for (let count = 0; count <= 20; count++) {
	if (count % 2 === 0){
		continue; //tells the code to continue to the next iteration of the loop
	}

	//the current value of number is printed out if the remainder is not equal to 0
	console.log ('Continue and Break: ' + count);


	//if the current value of count is greater than 10
	if (count>10)
		break; //tells the code to terminate the loop even if the condition is satisfied.
}


let name = 'Alexandro';

for (let i = 0; i < name.length; i++) {
	console.log(name[i]);

	if (name [i] .toLowerCase() === 'a') {
		console.log('continue to the next iteration');
		continue;
	}

	if (name[i] .toLowerCase() === 'd') {
		break;
	} 
}
